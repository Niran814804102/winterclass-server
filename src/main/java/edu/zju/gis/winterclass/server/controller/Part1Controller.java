package edu.zju.gis.winterclass.server.controller;

import edu.zju.gis.winterclass.server.base.Page;
import edu.zju.gis.winterclass.server.base.Result;
import edu.zju.gis.winterclass.server.service.part1Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin
@Controller
@RequestMapping("/part1")
public class Part1Controller {
    @Autowired
    part1Server service;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public Result selectByPage(Page page) {
        return Result.success().setBody(service.selectAllByPage(page));
    }
}
