package edu.zju.gis.winterclass.server.base;


import edu.zju.gis.winterclass.server.constant.CodeConstants;

/**
 * @author Hu
 * @date 2018年4月7日
 * updated by yanlong_lee@qq.com 2018/08/01
 */
public class Result<T> {

    private T body;
    private Integer code;
    private String message;

    public Result() {
        super();
    }

    public Result(Integer code) {
        super();
        this.code = code;
    }

    public Result(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Result(T body, Integer code, String message) {
        this.body = body;
        this.code = code;
        this.message = message;
    }


    public static <T> Result<T> create(Integer code) {
        return new Result<T>(code);
    }

    public static <T> Result<T> success() {
        return create(CodeConstants.SUCCESS);
    }

    public static <T> Result<T> error() {
        return create(CodeConstants.SYSTEM_ERROR);
    }

    public static Result<String> error(String message) {
        Result<String> result = create(CodeConstants.SYSTEM_ERROR);
        result.setMessage(message);
        return result;
    }

    public T getBody() {
        return body;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Result setBody(T body) {
        this.body = body;
        return this;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }
}
