package edu.zju.gis.winterclass.server.service;

import edu.zju.gis.winterclass.server.base.Page;
import edu.zju.gis.winterclass.server.model.part1;

import java.util.List;

public interface part1Server {
    List<part1> selectAll();
    Page<part1> selectAllByPage(Page page);
}
