package edu.zju.gis.winterclass.server.service.imp;

import com.github.pagehelper.PageHelper;
import edu.zju.gis.winterclass.server.base.Page;
import edu.zju.gis.winterclass.server.mapper.part1Mapper;
import edu.zju.gis.winterclass.server.model.part1;
import edu.zju.gis.winterclass.server.service.part1Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class part1ServerImp implements part1Server {
    @Autowired
    part1Mapper mapper;

    public List<part1> selectAll() {
        return mapper.selectAll();
    }

    public Page<part1> selectAllByPage(Page page) {
        PageHelper.startPage(page.getPageNo(), page.getPageSize());
        return new Page<part1>(mapper.selectAll());
    }
}
