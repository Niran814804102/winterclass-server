package edu.zju.gis.winterclass.server.mapper;

import edu.zju.gis.winterclass.server.model.part1;

import java.util.List;

public interface part1Mapper {
    int deleteByPrimaryKey(Integer gid);

    int insert(part1 record);

    int insertSelective(part1 record);

    part1 selectByPrimaryKey(Integer gid);

    List<part1> selectAll();

    int updateByPrimaryKeySelective(part1 record);

    int updateByPrimaryKey(part1 record);
}